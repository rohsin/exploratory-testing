### Scenarios Tried: ###
* Tried creating hierarchy with grandparent - parent- child with multiple files
* Tried adding parent as a child to another parent
* Tried making changes in multiple browser windows
* Tried adding different types of pages like files, blogs etc
* Tried adding/dragging pages outside the grandparent space
* Tried changing system time zone to verify history is not changed
* Tried expanding and collapsing of multiple parents and grandparent
* Tried adding page names with numbers and alphanumeric characters
* Tried adding pages to sub-parent page while creating new pages
* Tried adding pages with the same name while adding new page
* Login/Logout to verify changes are saved properly
* Auto-save is browser is closed by mistake
* Pages added should be same as that in Hierarchy View
* Reorder Pages button should navigate to Hierarchy View
* Pages for particular space should be displayed only

### Bugs: ###
- Page can be added out of grandparent(space) while adding a new page (Set new parent page as blank) 
- Parent with no child is displayed with expand/collapse icon
- Expand/collapse has issues
- UI icons for File Lists, Decision Logs etc is also similar to blank pages in the Hierarchy View
- A-Z alphabetical order is not maintained when dragging and dropping (intermittent)
- No auto-scroll while drag-n-drop if number of files are more than the browser window

### To be tested further: ###
- Permissions of pages with multiple user-types
- Adding all types of pages like JIRA reports, Meeting Notes, Requirements etc
- Would like to understand more about the implementation/use-case of the functionality so as to explore more issues and analyse risks involved with the same 
- Testing on Mobile Apps depending upon the implementation of the functionality
- Visibility and access (view/edit) of pages from different users




